# 巨应动态壁纸

## 第一款完整的开源 PC 动态壁纸软件，免费版 wallpaper engine

- **无流氓、极简 UI、单进程、省资源、无恶意弹窗无骚扰、纯净，只有核心功能，毕竟我自己也是要用的...**

### 功能

- [x] 视频动态壁纸 （已发布）
- [x] 创建/编辑壁纸 （已发布）
- [x] 其他程序全屏时暂停 （已发布）
- [x] 多屏支持 （已发布）
- [x] 音源设置 （已发布）
- [ ] 网页动态壁纸 （计划中）
- [ ] 应用程序动态壁纸 （计划中）

### 下载地址

[官网](https://mscoder.cn/products/LiveWallpaper.html)  
[动态壁纸下载](https://www.microsoft.com/store/apps/9MV8GK87MZ05)  
[壁纸商店下载](https://www.microsoft.com/store/apps/9PNN27P9SS38)

### 关于本项目：

- 动态壁纸WPF实现版本，类似 wallpaper engine。
- 只支持 Win10。
- 目的是动态壁纸,所以UI能省既省。
- 有 Win10 内购只是一种捐赠形式。
- 多语言是谷歌翻译的。有好心人可以 pr 这里 [链接](https://github.com/MscoderStudio/LiveWallpaper/blob/master/LiveWallpaper/Res/Languages/en.json)。
- 本仓库是完整项目。如果只是想了解动态壁纸原理，请看[这里](https://github.com/MscoderStudio/LiveWallpaperEngine)。
- 如果你觉得本项目对你有帮助，请 star 支持一下~


### 关于开源：

- 完全开源毫无保留，开源的初衷是希望大家一起完善开源项目，不一定要提交代码，提交 Issue 也是一种支持。

### 编译环境：

- VS2019
- Win10 如果某些依赖库找不到，可能是因为最新版本没有来得及上传到 nuget，需要手动编译或者提交 issue 告知我
- 需要安装 Windows10 Software Development Kit SDK。
- 如果你的系统盘不是 C 盘还需要手动修改路径。右键 LiveWallpaper->编辑项目文件。dll 位置参考 https://www.mscoder.cn/program/wpf-reference-uwp/

### 感谢：
* 感谢所有的issue提交者
* 感谢所有的源码[贡献者](/Docs/Contribution.md)

---

### 广告区域:

**C#全栈开发**：191034956  
**全球 it 互联交友**: 665127101
（只允许单身码农和妹子加入，初衷是给程序员多一种认识异性的渠道）

---

## 视频教程：

### [软件使用教程](https://www.bilibili.com/video/av48407705/)

## 软件截图

### 客户端

[![client](https://github.com/WallpaperTools/WallpaperTool/raw/master/screenshots/client.png)](https://github.com/WallpaperTools/WallpaperTool/blob/master/screenshots/client.png)

### 商店

[![store](https://github.com/WallpaperTools/WallpaperTool/raw/master/screenshots/store.png)](https://github.com/WallpaperTools/WallpaperTool/blob/master/screenshots/store.png)

### 早期 Demo （gif 文件略大，请耐心等候）

[![早期Demo](https://github.com/WallpaperTools/WallpaperTool/raw/master/screenshots/example.gif)](https://github.com/WallpaperTools/WallpaperTool/blob/master/screenshots/example.gif)
